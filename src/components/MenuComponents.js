import React, { Component } from 'react';
import { Card,CardImg,CardImgOverlay,CardTitle, CardText,CardBody, Media} from 'reactstrap';

class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDish:null
        };
    }

    onDishSelect(dish) {
      this.setState({ selectedDish: dish});
  }

  renderDish(dish) {
    if (dish != null)
        return(
            <Card>
              
                <CardImg width="100%"  src={dish.image} alt={dish.name} />
                <CardBody>
                  <CardTitle>{dish.name}</CardTitle>
                  <CardText>{dish.description}</CardText>
                
                </CardBody>
                
            </Card>
            
          
        );
         
        
    else
        return(
            <div></div>
        );
}
//TO generate the comment component
generateCommentRow(comment)
{
  if(comment !== undefined )
  return comment.map(data=>{
    return(<div>
      {data.comment}
    </div>)
  })
}

rendercmd(dish) {
  if (dish != null)
      return(
          
       <Media body>
                  <Media heading>
                  Comments
                  {this.generateCommentRow(dish.comments)}
            {console.log(dish.comments)}
                  </Media>

    </Media>
              
        
      );
       
      
  else
      return(
          <div></div>
      );
}


    render() {
        const menu = this.props.dishes.map((dish) => {
            return (
              
              <div key={dish.id} className="col-12 col-md-5 m-1">
                <Card  onClick={()=> this.onDishSelect(dish)} tag="li">
               
                      <CardImg width="100%"   src={dish.image} alt={dish.name} />
            
                  <CardImgOverlay>
                    < CardTitle>{dish.name}</ CardTitle>
                  </CardImgOverlay>
                </Card>
                
              </div>
            );
            
        });

        return (
          <div className="container">
            <div className="row">
                  {menu}
            </div>
            <div className="row">
              <div className="col">
              {this.renderDish(this.state.selectedDish)}
              </div>
              <div className="col">
              {this.rendercmd(this.state.selectedDish)}
              </div>
            </div>
          </div>
        );
    }
}

export default Menu;